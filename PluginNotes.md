



# Plugins
   *This is a list of plugins and notes about them. Examples of the plugins will be below and also in the Example Jenkins file*

* [Credentials Binding Plugin](https://wiki.jenkins.io/display/JENKINS/Credentials+Binding+Plugin)
     * Allows credentials to be bound to environment variables for use from miscellaneous build steps.
     * Useful for abstracting credientials stored/access via jenkins to a build pipeline. 

* [Config File Provider Plugin](https://wiki.jenkins.io/display/JENKINS/Config+File+Provider+Plugin)
     * Adds the ability to provide configuration files (i.e., settings.xml for maven, XML, groovy, custom files, etc.) loaded through the Jenkins UI which will be copied to the job's workspace.
     * Can do things like token replacement based on set environment variables. 

* [Script Security Plugin](https://wiki.jenkins.io/display/JENKINS/Script+Security+Plugin)
     * Allows Jenkins administrators to control what in-process scripts can be run by less-privileged users.

* [File Operations Plugin](https://wiki.jenkins.io/display/JENKINS/File+Operations+Plugin)
     * This plugin's main goal is to provide File Operations as Build Step.
     * Common file operations: zip/unzip, tar/untar, download from url,... 

* [Publish xUnit Test Results](https://plugins.jenkins.io/xunit)
     * This plugin makes it possible to publish the test results of an execution of a testing tool in Jenkins. 
     * Example:   
     xunit testTimeMargin: '3000',   
                thresholdMode: 1,  
                thresholds: [failed(failureNewThreshold: '1', failureThreshold: '1', unstableNewThreshold: '1', unstableThreshold: '1'), skipped(failureThreshold: '1', unstableNewThreshold: '1', unstableThreshold: '1')],   
                tools: [JUnit(deleteOutputFiles: true, failIfNotNew: true, pattern: '***/*.xml', skipNoTestFiles: false, stopProcessingIfError: true)]



