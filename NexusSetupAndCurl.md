// Sonatype Install
// -------------------------------------------------------------------------
// * this assumes install on REHL 7.x or CentOS 7.x

// Requires Java 1.8
sudo yum install java-1.8.0-openjdk.x86_64

// create service account
sudo adduser nexus 

// Download Nexus (here we are using 3.0.2) to a scratch directory
tar zxvf nexus-3.0.2-02-unix.tar.gz
sudo mv nexus-3.0.2-02 nexus
sudo mv nexus /opt/nexus 
sudo chown -R nexus:nexus /opt/nexus

// Set Nexus service account in /app/nexus/bin/nexus.rc
// run_as_user="nexus"

// Create the systemd starup file /etc/systemd/system/nexus.service
// [Unit]
// Description=nexus service
// After=network.target
  
// [Service]
// Type=forking
// ExecStart=/opt/nexus/bin/nexus start
// ExecStop=/opt/nexus/bin/nexus stop
// User=nexus
// Restart=on-abort
  
// [Install]
// WantedBy=multi-user.target

// Register and Start Nexus 
// sudo systemctl daemon-reload
// sudo systemctl enable nexus.service
// sudo systemctl start nexus.service

// Verify install starts up ok
tail /opt/nexus/data/log/nexus.log 

// Example uploading via curl 

curl -v -u <username>:<password> --upload-file artifact.zip https://<nexus-server>/repository/<repository_name>/artifact.zip